from django.shortcuts import render
from django.template.loader import get_template
from django.http import HttpResponse
from django.template import Context
import re
from evard.models import *

# Create your views here.

# eVarD homepage
def homepage(request):
	t = get_template('index.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)

# eVarD contactpage
def contactpage(request):
	t = get_template('contact.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)


# eVarD PMID search page
def searchpmidpage(request):
	t = get_template('searchpmid.html')
	
	found=False
	context_data_dict=dict()
	pmid_list=list()

	# get the valid pmids in pmid_list
	if 'pmids' in request.GET:
		pmids_str = request.GET['pmids']
		if pmids_str:
			found=True
			context_data_dict['found']=found	
			pmids=re.split(',| ',pmids_str.strip())
			for pmid in pmids:
				if pmid.strip()!='':
					try:
						newpmid=int(pmid.strip())
						if newpmid not in pmid_list:
							pmid_list.append(newpmid)
					except ValueError:
						print "not an int"

	
	# seach database for PMIDs and refine the pmid_list
	tuples=list()
	pmidDict=dict()
	for pmid in pmid_list:
		dataintuples=dict()
		pmidobj = Pmid.objects.filter(pmid=pmid, isextracted=1)		
		triplets = Triplets.objects.filter(pmid=pmidobj)
		for triplet in triplets:
			dataintuples=dict()
			dataintuples['pmid']=pmid
			dataintuples['mut']=triplet.mutation.mutation
			dataintuples['gene']=triplet.gene.gene
			dataintuples['disease']=triplet.disease.disease
			dataintuples['location']=triplet.extractlocation
			tuples.append(dataintuples)
			pmidDict[pmid]=pmid


	stats=dict()
	stats['abstract_length']=len(pmidDict)
	stats['tuples_length']=len(tuples)
	context_data_dict['tuples']=tuples
	context_data_dict['stat']=stats


	html = t.render(Context(context_data_dict))
	return HttpResponse(html)


# eVarD contactpage
def sentenceviewpage(request, pmid):
	t = get_template('sentenceview.html')
	
	context_data_dict=dict()

	tuples=list()
	pmidobj = Pmid.objects.filter(pmid=pmid, isextracted=1)		
	triplets = Triplets.objects.filter(pmid=pmidobj)
	for triplet in triplets:
		dataintuples=dict()
		dataintuples['pmid']=pmid
		dataintuples['mut']=triplet.mutation.mutation
		dataintuples['gene']=triplet.gene.gene
		dataintuples['disease']=triplet.disease.disease
		tuples.append(dataintuples)

	context_data_dict['tuples']=tuples
	context_data_dict['pmid']=pmid

	#sentences
	sentences=Abstracts.objects.filter(pmid=pmidobj).order_by('sentno')
	sentencelist=list()
	for sent in sentences:
		sentencelist.append(sent.text)

	context_data_dict['taggedsentences']=sentencelist

	html = t.render(Context(context_data_dict))
	return HttpResponse(html)