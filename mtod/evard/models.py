from django.db import models


# Create your models here.

class Pmid(models.Model):
	pmid=models.IntegerField()
	isextracted=models.IntegerField() #1=yes, 2=no, 3=processed, but not nothing extracted

	def __unicode__(self):
		return u'%s' % self.pmid

class Abstracts(models.Model):
	pmid=models.ForeignKey(Pmid)
	sentno=models.IntegerField()
	sentcategory=models.CharField(max_length=20)
	text=models.CharField(max_length=1000)

	def __unicode__(self):
		return u'%s' % self.text

class Gene(models.Model):
	pmid=models.ForeignKey(Pmid)
	gene=models.CharField(max_length=100)
	normalizedgene=models.CharField(max_length=100)
	sentno=models.IntegerField()
	startpos=models.IntegerField()
	endpos=models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.gene

class Mutation(models.Model):
	pmid=models.ForeignKey(Pmid)
	mutation=models.CharField(max_length=150)
	normalizedmutation=models.CharField(max_length=150)
	wtype3=models.CharField(max_length=50)
	mtype3=models.CharField(max_length=50)
	wtype1=models.CharField(max_length=1)
	mtype1=models.CharField(max_length=1)
	mpos=models.CharField(max_length=30)
	sentno=models.IntegerField()
	startpos=models.IntegerField()
	endpos=models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.mutation

class Disease(models.Model):
	pmid=models.ForeignKey(Pmid)
	disease=models.CharField(max_length=300)
	normalizeddisease=models.CharField(max_length=300)
	sentno=models.IntegerField()
	startpos=models.IntegerField()
	endpos=models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.disease

class Triplets(models.Model):
	pmid=models.ForeignKey(Pmid)
	mutation=models.ForeignKey(Mutation)
	gene=models.ForeignKey(Gene)
	disease=models.ForeignKey(Disease)
	isnegative=models.IntegerField()
	extractlocation=models.CharField(max_length=20)
	extractsent=models.IntegerField()
	extractmethod=models.CharField(max_length=20)
	outcome=models.CharField(max_length=300)

	def __unicode__(self):
		return u'%s, %s, %s' % (self.mutation, self.gene, self.disease)

class Additional(models.Model):
	pmid=models.ForeignKey(Pmid)
	patient=models.CharField(max_length=100)
	population=models.CharField(max_length=100)

	def __unicode__(self):
		return r'%s, %s' % (self.patient, self.population)
