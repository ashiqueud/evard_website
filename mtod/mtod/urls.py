from django.conf.urls import patterns, include, url
from madix.views import *
from django.views.generic import RedirectView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mtod.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', homepage),
    url(r'^contact/$', contactpage, name='contact'),
    url(r'^searchpmid/([0-9,]+)?$', searchpmidpage, name='searchpmid'),
    url(r'^search/$', searchpage),
    url(r'^download/(\d)$', downloadpage, name='download'),
    url(r'^db/$', dbpage, name='db'),
    url(r'^dbdownload/(\d)$', fulldownloadpage, name='dbdownload'),
    url(r'^sentences/(\d+)/$', sentenceviewpage, name='sentences'),
    url(r'^showtext/(.*)/$', showtextpage, name='showtext'),
    #url(r'^$', RedirectView.as_view(url='/')),
    #url(r'^/$', homepage),

)
