import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mtod.settings")

import sys
import re
from django.db import transaction

import django
django.setup() # Needed in laptop

from madix.models import *

## command line arguments
# taking the input filename
narg = len(sys.argv)

if narg<3:
	print "...failed! enter triplet filename and abstract filename"
	sys.exit()
else:
	t_filename=sys.argv[1]
	a_filename=sys.argv[2]



pmids=dict()

def isTripletValid(pieces):
	# mutation
	if len(pieces[1])>150:
		return False
	if pieces[1].strip()=='' or pieces[2].strip()=='' or pieces[3].strip()=='':
		return False
	#if not re.search(r'cancer|carcinoma|leukemia|CRC|HCC|NSCLC|tumor', pieces[3], re.M|re.I):
		#print pieces[3]
	#	return False
	if pieces[22].strip()=='':
		return False
	if pieces[19]=='P' or pieces[19]=='D':
		if pieces[13]=='' or pieces[14]=='' or pieces[17]=='':
			if pieces[15]=='' or pieces[16]=='' or pieces[17]=='':
				#print '\t'.join(pieces)
				return False

	if pieces[19]=='A':
		return False


	return True


def getCanonicalForm(pieces):
	
	if pieces[19]=='I' or pieces[19]=='X' or pieces[19]=='RS' or pieces[19]=='IIVS' or pieces[19]=='XIVS':
		return pieces[1].upper()
	elif pieces[19]=='DIVS':
		temp=re.sub(r'\s+','',pieces[1])
		temp=re.sub(r'&gt;','',temp)
		temp=re.sub(r'/','',temp)
		return temp.upper()
	elif pieces[19]=='P' or pieces[19]=='D':
		if pieces[13]!='' and pieces[14]!='' and pieces[17]!='':
			canonmut=pieces[13]+pieces[17]+pieces[14]
		elif pieces[15]!='' and pieces[16]!='' and pieces[17]!='':
			canonmut=pieces[15]+pieces[17]+pieces[16]
		else:
			print "ERROR"
		return canonmut.upper()


def get3letterFormat(mutation, letter1, mtype, original3letter):
	letter3=''
	mapping=dict([('A','ALA'),('G','GLY'),('L','LEU'),('M','MET'),('F','PHE'),('W','TRP'), \
					('K','LYS'),('Q','GLN'),('E','GLU'),('S','SER'),('P','PRO'),('V','VAL'), \
					('I','ILE'),('C','CYS'),('Y','TYR'),('H','HIS'),('R','ARG'),('N','ASN'), \
					('D','ASP'),('T','THR'),('X','XAA'),('Z','GLX'),('B','ASX')])

	if re.match(r'^([A-Z0-9\s]+)$',mutation,re.M) and (mtype=='P' or mtype=='D'):
		#print "matched: "+mutation
		if mapping.has_key(letter1.upper()):			
			letter3=mapping[letter1.upper()]
			#print "found: "+letter3		
	#else:
	#	print "not matched: "+mutation
	if letter3=='':
		return original3letter
	return letter3


def getLocationPriorityVal(location):
	priorities={'Result':1, 'Conclusion':2, 'Method':3, 'Title':4, 'Introduction':5, 'Undefined':6}
	return priorities[location]


@transaction.atomic
def AbstractEntry():
	global pmids

	#22969927~@~PATIENT	9 patients, 1 tumor
	#22969927~@~POPULATION	
	#22969927~@~META-ANALYSIS	
	#22969927~@~REVIEW	
	#22969927~@~SENTENCES

	pmid_abs_info=dict()

	fh=open(a_filename, "r")
	for line in fh:
		if line.strip()!='':
			ps=line.split('~@~')
			pmid=int(ps[0])
			rest=ps[1]

			if pmid_abs_info.has_key(pmid):
				pmid_abs_info[pmid]+='~@~'+rest
			else:
				pmid_abs_info[pmid]=rest

	fh.close()
	

	for k in pmid_abs_info.keys():
		pmid=int(k)
		values=pmid_abs_info[k].split('~@~')
		review=''
		patient=''
		population=''
		meta=''
		sentences=''
		for value in values:
			pss=value.split('\t')
			tag=pss[0].strip()
			val=pss[1].strip()
			if tag=='PATIENT':
				patient=val
			elif tag=='POPULATION':
				#population=val
				pops=val.split(', ')
				pops=list(set(pops))
				population=', '.join(pops)
			elif tag=='REVIEW':
				review=val
			elif tag=='META-ANALYSIS':
				meta=val
			elif tag=='SENTENCES':
				sentences=val

		if pmid not in pmids.keys():			
			# pmid entry
			#if pmid==17917789:
			#	print "yes"
			pmidobj=Pmid.objects.create(pmid=pmid, isextracted=1)
			pmids[pmid]=pmid

		# additional entry
		
		additional=Additional.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
						review=review, \
						patient=patient, \
						population=population, \
						meta=meta)		
		
		sentences_ps=sentences.split('~#~')
		x=0
		for sentence in sentences_ps:
			sentence = re.sub(r'\s+',' ',sentence)			
			Abstracts.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
					sentno=x, \
					sentcategory='', \
					text=sentence)
			x+=1
		
	print str(len(pmid_abs_info))+' PMIDs entered.'

@transaction.atomic
def TripletEntry():
	
	global pmids

	fh=open(t_filename,"r")		
	x=0
	for line in fh:		
		if line.strip()!='':
			pieces=line.split('\t')

			#0-pmid  1-mutation  2-gene  3-disease  4-Xlocation  5-Xsent  6-Xtype  7-patient  8-meta  
			#9-review  10-posneg  11-population  12-mutlocation  13-w3  14-m3  15-w1  16-m1  
			#17-mpos  18-sign  19-mType 20-zphrase
			
			pmid=int(pieces[0])

			if isTripletValid(pieces):
				x+=1
				
				# gene entry				
				geneobj=Gene.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
						gene=pieces[2], \
						normalizedgene=pieces[2].upper(), \
						entrezids=pieces[24].strip(), \
						entrezsymbol=pieces[25].strip(), \
						sentno=0, \
						startpos=0, \
						endpos=0)
				
				# mutation entry
				#print pieces[13]
				#if pieces[13].strip()=='':
				#	mpos=0
				#else:
				mpos=pieces[17]
				pieces[13]= get3letterFormat(pieces[1], pieces[15], pieces[19].strip(), pieces[13])
				pieces[14]= get3letterFormat(pieces[1], pieces[16], pieces[19].strip(), pieces[14])
				norm_mutation=getCanonicalForm(pieces)
				#print norm_mutation+'\t'+pieces[19]
				
				
				mutationobj=Mutation.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
									mutation=pieces[1], \
									normalizedmutation=norm_mutation, \
									wtype3=pieces[13], \
									mtype3=pieces[14], \
									wtype1=pieces[15], \
									mtype1=pieces[16], \
									mpos=mpos, \
									msign=pieces[18], \
									sentno=0, \
									startpos=0, \
									endpos=0)
				# disease entry
				diseaseobj=Disease.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
									disease=pieces[3], \
									normalizeddisease=pieces[3].upper(), \
									DOID=pieces[22], \
									DOIDname=pieces[23], \
									sentno=0, \
									startpos=0, \
									endpos=0)
				# triplet entry
				if pieces[10].strip()=='':
					isnegative=0
				else:
					isnegative=1

				if pieces[21].strip()=='':
					isstudy=0
				else:
					isstudy=1
			
				extSent=int(pieces[5].strip().split('-')[1])
				triplet=Triplets.objects.create(pmid=Pmid.objects.get(pmid=pmid), \
									mutation=Mutation.objects.filter(mutation=pieces[1], pmid=Pmid.objects.get(pmid=pmid))[0], \
									gene=Gene.objects.filter(gene=pieces[2], pmid=Pmid.objects.get(pmid=pmid))[0], \
									disease=Disease.objects.filter(disease=pieces[3], pmid=Pmid.objects.get(pmid=pmid))[0], \
									isnegative=isnegative, \
									isstudy=isstudy, \
									extractlocation=pieces[4], \
									extractlocpriority=getLocationPriorityVal(pieces[4]), \
									extractsent=extSent, \
									extractmethod=pieces[6], \
									outcome=pieces[20]) 
				
				#'''

	fh.close()
	print str(x)+' Triplets entered.'


if __name__ == '__main__':	

	AbstractEntry()
	TripletEntry()
		




