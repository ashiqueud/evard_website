import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mtod.settings")

import sys
import re
from django.db import transaction

import django
from madix.models import *




@transaction.atomic
def generateTripletViewCSV():

	print 'PMID'+'\t'+'MUTATION'+'\t'+'NORMALIZED_MUTATION'+'\t'+'GENE'+'\t'+'DISEASE'+'\t'+'LOCATION'+'\t'+'OUTCOME'+'\t'+'WILD-TYPE'+'\t'+'MUTANT-TYPE'+'\t'+'WILD-TYPE'+'\t'+'MUTANT-TYPE'+'\t'+'MUTANT_POSITION'
	tripletobjs=Triplets.objects.all()
	for tripletobj in tripletobjs:
		pmid=tripletobj.pmid.pmid
		normalizedmutation=tripletobj.mutation.normalizedmutation
		mutation=tripletobj.mutation.mutation
		gene=tripletobj.gene.gene
		disease=tripletobj.disease.disease
		location=tripletobj.extractlocation
		sentno=int(tripletobj.extractsent)+1
		outcome=tripletobj.outcome.strip()
		w3=tripletobj.mutation.wtype3
		m3=tripletobj.mutation.mtype3
		w1=tripletobj.mutation.wtype1
		m1=tripletobj.mutation.mtype1
		mpos=tripletobj.mutation.mpos
		print str(pmid)+'\t'+mutation+'\t'+normalizedmutation+'\t'+gene+'\t'+disease+'\t'+location+'\t'+outcome+'\t'+w3+'\t'+m3+'\t'+w1+'\t'+m1+'\t'+str(mpos)


@transaction.atomic
def generatePmidViewCSV():

	print 'PMID'+'\t'+'PATIENTS'+'\t'+'POPULATION'+'\t'+'REVIEW'+'\t'+'META-ANALYSIS'

	additionalobjs=Additional.objects.all()
	for additional in additionalobjs:		
		pmid=additional.pmid.pmid
		patient=additional.patient
		population=additional.population
		review=additional.review
		meta=additional.meta
		print str(pmid)+'\t'+patient+'\t'+population+'\t'+review+'\t'+meta		


if __name__ == '__main__':	

	#generateTripletViewCSV()
	generatePmidViewCSV()
		




