import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mtod.settings")

import sys
import re
from django.db import transaction

import django
#django.setup() # Needed in laptop
from madix.models import *




@transaction.atomic
def getUniqueTriplets():

	ivs_dict=dict()
	regular_dict=dict()
	rs_dict=dict()
	all_dict=dict()
	mut_dict=dict()

	x=0
	tripletobjs=Triplets.objects.all()
	for tripletobj in tripletobjs:
		pmid=tripletobj.pmid.pmid
		mutation=tripletobj.mutation.mutation
		normalizedmutation=tripletobj.mutation.normalizedmutation
		gene=tripletobj.gene.gene
		disease=tripletobj.disease.disease
		location=tripletobj.extractlocation
		sentno=int(tripletobj.extractsent)+1
		outcome=tripletobj.outcome.strip()
		w3=tripletobj.mutation.wtype3
		m3=tripletobj.mutation.mtype3
		w1=tripletobj.mutation.wtype1
		m1=tripletobj.mutation.mtype1
		mpos=tripletobj.mutation.mpos

		if re.match(r'^(RS|SS)\d+$',normalizedmutation,re.M):
			x+=1
			key=normalizedmutation+','+gene+','+disease
			rs_dict[key]=key
			all_dict[key]=key
			mut_dict[normalizedmutation]=normalizedmutation
		elif w3=='IVS' or m3=='IVS':
			x+=1
			key=normalizedmutation+','+gene+','+disease
			ivs_dict[key]=key
			all_dict[key]=key
			mut_dict[normalizedmutation]=normalizedmutation
		else:
			x+=1
			made_mut=w3+m3+w1+m1+mpos
			#key=made_mut+','+gene+','+disease
			key=normalizedmutation+','+gene+','+disease
			regular_dict[key]=key
			all_dict[key]=key
			#mut_dict[made_mut]=made_mut
			mut_dict[normalizedmutation]=normalizedmutation

	print x
	count=len(rs_dict)+len(ivs_dict)+len(regular_dict)
	#print "RS "+str(len(rs_dict))
	#print "IVS "+str(len(ivs_dict))
	#print "M "+str(len(regular_dict))
	print "unique triplet: "+str(len(all_dict))
	print "unique mut: "+str(len(mut_dict))

	
		



if __name__ == '__main__':	

	getUniqueTriplets()
		




