from django.shortcuts import render
from django.template.loader import get_template
from django.http import HttpResponse
from django.template import Context
from django.utils.encoding import smart_str
import re
import csv
import os
from django.http import HttpResponse
from madix.models import *

# Create your views here.

# eVarD homepage
def homepage(request):
	t = get_template('index.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)

# eVarD contactpage
def contactpage(request):
	t = get_template('contact.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)

# eVarD homepage
def dbpage(request):
	t = get_template('db.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)

# eVarD contactpage
def downloadpage(request, flag):
	'''
	t = get_template('contact.html')
	data_dict=dict()	
	html = t.render(Context(data_dict))
	return HttpResponse(html)
	'''

	# Create the HttpResponse object with the appropriate CSV header.
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="result.csv"'
	writer = csv.writer(response)

	pwd=os.getcwd()	

	if int(flag)==1:
		filepath=pwd+'/static/downloadcsv'
		#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/downloadcsv'
		#f = open('/static/downloadcsv','r')
		f = open(filepath,'r')
	else:
		filepath=pwd+'/static/downloadcsvpmid'
		#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/downloadcsvpmid'
		#f = open('/static/downloadcsvpmid','r')
		f = open(filepath,'r')
	for line in f:
		if line.strip()!='':
			row=line.split('\t')
			writer.writerow(row)

	f.close()
	return response

def fulldownloadpage(request, flag):

	# Create the HttpResponse object with the appropriate CSV header.
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="result.csv"'
	writer = csv.writer(response)
	pwd=os.getcwd()	
	if int(flag)==1:
		#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/fullDBtriplets.csv'
		filepath=pwd+'/static/fullDBtriplets.csv'
	else:
		#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/fullDBpmids.csv'
		filepath=pwd+'/static/fullDBpmids.csv'
	f = open(filepath,'r')
	for line in f:
		if line.strip()!='':
			row=line.split('\t')
			writer.writerow(row)
	f.close()
	return response


	'''
	pwd=os.getcwd()
	filepath=pwd+'/static/downloadcsv/'
	file_name='fullDBtriplets.csv'
	#path_to_file='./'
	response = HttpResponse(content_type='application/force-download')
	response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
	response['X-Sendfile'] = smart_str(filepath)
	# It's usually a good idea to set the 'Content-Length' header too.
	# You can also set any other required headers: Cache-Control, etc.
	return response
	'''

def searchpage(request):
	t = get_template('search.html')

	found=False

	# data
	context_data_dict=dict()
	stat=dict()
	tuples=list()
	pmids_found=dict()
	mutations_found=dict()

	lowConfidenceMethods=['MA','TI','CO']
	noConfidenceMethods=['COMEN']

	if 'mutstring' in request.GET:
		mutstr=request.GET['mutstring']
		mutstr=re.sub(r'\s+','',mutstr)
		muts=re.split(',',mutstr.strip())
		muts=map(lambda x:x.upper(),muts)

		#for m in muts:
		#	print m
		mutobjs = Mutation.objects.filter(normalizedmutation__in=muts)
		#for mutobj in mutobjs:
		#	print "objfound"
		triplets = Triplets.objects.filter(mutation__in=mutobjs)
		for triplet in triplets:
			dataintuples=dict()
			dataintuples['pmid']=triplet.pmid.pmid
			dataintuples['mut']=re.sub(r'&gt ;','>',triplet.mutation.normalizedmutation)
			dataintuples['gene']=triplet.gene.gene
			dataintuples['entrez']=triplet.gene.entrezids
			dataintuples['entrezsymbol']=triplet.gene.entrezsymbol
			dataintuples['disease']=triplet.disease.disease
			dataintuples['doid']=triplet.disease.DOID
			dataintuples['doidname']=triplet.disease.DOIDname
			dataintuples['location']=triplet.extractlocation
			dataintuples['sentno']=int(triplet.extractsent)+1
			dataintuples['outcome']=triplet.outcome
			dataintuples['isstudy']=triplet.isstudy
			dataintuples['isnegative']=triplet.isnegative
			dataintuples['exmethod']=triplet.extractmethod

			mutations_found[dataintuples['mut']]=dataintuples['mut']
			
			negativeStr=''
			if dataintuples['isnegative']==1:
				negativeStr=' (NEG)'

			if dataintuples['isstudy']==1:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in noConfidenceMethods:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in lowConfidenceMethods:
				dataintuples['confidence']='LOW'+negativeStr
			else:
				dataintuples['confidence']='HIGH'+negativeStr

			if dataintuples not in tuples:
				tuples.append(dataintuples)
				if not pmids_found.has_key(triplet.pmid.pmid):
					pmids_found[triplet.pmid.pmid]=triplet.pmid.pmid


	if 'genes' in request.GET:
		genestr=request.GET['genes']
		genestr=re.sub(r'\s+','',genestr)
		genes=re.split(',',genestr.strip())
		genes=map(lambda x:x.upper(),genes)

		geneobjs = Gene.objects.filter(normalizedgene__in=genes)
		triplets = Triplets.objects.filter(gene__in=geneobjs)
		for triplet in triplets:
			dataintuples=dict()
			dataintuples['pmid']=triplet.pmid.pmid
			dataintuples['mut']=re.sub(r'&gt ;','>',triplet.mutation.normalizedmutation)
			dataintuples['gene']=triplet.gene.gene
			dataintuples['entrez']=triplet.gene.entrezids
			dataintuples['entrezsymbol']=triplet.gene.entrezsymbol
			dataintuples['disease']=triplet.disease.disease
			dataintuples['doid']=triplet.disease.DOID
			dataintuples['doidname']=triplet.disease.DOIDname
			dataintuples['location']=triplet.extractlocation
			dataintuples['sentno']=int(triplet.extractsent)+1
			dataintuples['outcome']=triplet.outcome
			dataintuples['isstudy']=triplet.isstudy
			dataintuples['isnegative']=triplet.isnegative
			dataintuples['exmethod']=triplet.extractmethod

			mutations_found[dataintuples['mut']]=dataintuples['mut']

			negativeStr=''
			if dataintuples['isnegative']==1:
				negativeStr=' (NEG)'

			if dataintuples['isstudy']==1:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in noConfidenceMethods:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in lowConfidenceMethods:
				dataintuples['confidence']='LOW'+negativeStr
			else:
				dataintuples['confidence']='HIGH'+negativeStr

			if dataintuples not in tuples:
				tuples.append(dataintuples)
				if not pmids_found.has_key(triplet.pmid.pmid):
					pmids_found[triplet.pmid.pmid]=triplet.pmid.pmid

	if 'diseases' in request.GET:
		diseasestr=request.GET['diseases']
		diseases=re.split(', ',diseasestr.strip())
		diseases=map(lambda x:x.upper(),diseases)

		diseaseobjs = Disease.objects.filter(normalizeddisease__in=diseases)
		triplets = Triplets.objects.filter(disease__in=diseaseobjs)
		for triplet in triplets:
			dataintuples=dict()
			dataintuples['pmid']=triplet.pmid.pmid
			dataintuples['mut']=re.sub(r'&gt ;','>',triplet.mutation.normalizedmutation)
			dataintuples['gene']=triplet.gene.gene
			dataintuples['entrez']=triplet.gene.entrezids
			dataintuples['entrezsymbol']=triplet.gene.entrezsymbol
			dataintuples['disease']=triplet.disease.disease
			dataintuples['doid']=triplet.disease.DOID
			dataintuples['doidname']=triplet.disease.DOIDname
			dataintuples['location']=triplet.extractlocation
			dataintuples['sentno']=int(triplet.extractsent)+1
			dataintuples['outcome']=triplet.outcome
			dataintuples['isstudy']=triplet.isstudy
			dataintuples['isnegative']=triplet.isnegative
			dataintuples['exmethod']=triplet.extractmethod

			mutations_found[dataintuples['mut']]=dataintuples['mut']

			negativeStr=''
			if dataintuples['isnegative']==1:
				negativeStr=' (NEG)'

			if dataintuples['isstudy']==1:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in noConfidenceMethods:
				dataintuples['confidence']='?'

			elif dataintuples['exmethod'] in lowConfidenceMethods:
				dataintuples['confidence']='Low'+negativeStr
			else:
				dataintuples['confidence']='High'+negativeStr

			if dataintuples not in tuples:
				tuples.append(dataintuples)
				if not pmids_found.has_key(triplet.pmid.pmid):
					pmids_found[triplet.pmid.pmid]=triplet.pmid.pmid

	population_pmids=list()
	review_meta_pmids=list()
	for dataintuple in tuples:
		pmid=dataintuple['pmid']
		additional=Additional.objects.filter(pmid=Pmid.objects.filter(pmid=pmid))[0]
		
		dataintuple['patient']=additional.patient
		dataintuple['population']=additional.population
		if additional.population!='':
			if pmid not in population_pmids:
				population_pmids.append(pmid)
		dataintuple['review']=additional.review
		if additional.review!='':
			if pmid not in review_meta_pmids:
				review_meta_pmids.append(pmid)
		dataintuple['meta']=additional.meta
		if additional.meta!='':
			if pmid not in review_meta_pmids:
				review_meta_pmids.append(pmid)
		

	if len(tuples)>0:
		found=True
		stat['tuplesize']=len(tuples)
		stat['pmidsize']=len(pmids_found)
		stat['poppmidsize']=len(population_pmids)
		stat['metareviewpmidsize']=len(review_meta_pmids)
		stat['mutationsize']=len(mutations_found)

	context_data_dict['found']=found
	context_data_dict['muts']=mutstr
	context_data_dict['genes']=genestr
	context_data_dict['diseases']=diseasestr
	context_data_dict['tuples']=tuples
	context_data_dict['stat']=stat
	context_data_dict['population_pmids']=','.join(map(str, population_pmids))
	context_data_dict['review_meta_pmids']=','.join(map(str, review_meta_pmids))

	pmids_list=""
	for k in pmids_found.keys():
		pmids_list+=str(k)+"~@~"
	context_data_dict['pmids']=pmids_list

	mutations_list=""
	for k in mutations_found.keys():
		mutations_list+=str(k)+"~@~"
	context_data_dict['mutations']=mutations_list

	# writing to CSV for downloading
	#os.path.join(BASE_DIR, "/media")
	pwd=os.getcwd()
	filepath=pwd+'/static/downloadcsv'
	#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/downloadcsv'
	#f = open('/static/downloadcsv','w')
	f = open(filepath,'w')
	line='PMID'+'\t'+'MUTATION'+'\t'+'GENE'+'\t'+'DISEASE'+'\t'+'EXTRACT_LOCATION'+'\t'+'SENT#'+'\t'+'OUTCOME'
	f.write(line+'\n')
	for dataintuples in tuples:
		line=str(dataintuples['pmid'])+'\t'+dataintuples['mut']+'\t'+dataintuples['gene']+'\t'+dataintuples['disease']+'\t'+dataintuples['location']+'\t'+str(dataintuples['sentno'])+'\t'+dataintuples['outcome'].strip()
		f.write(line+'\n')
	f.close()

	html = t.render(Context(context_data_dict))
	return HttpResponse(html)


# eVarD PMID search page
def searchpmidpage(request, argpmids):
	t = get_template('searchpmid.html')
	
	found=False
	context_data_dict=dict()
	pmid_list=list()



	# get the valid pmids in pmid_list
	if 'pmids' in request.GET:
		pmids_str = request.GET['pmids']
		if pmids_str:
			found=True
			context_data_dict['found']=found

			pmids_str=re.sub('\s+',',',pmids_str)
			pmids_str=re.sub('\n',',',pmids_str)
			pmids=re.split(',',pmids_str.strip())
			
			for pmid in pmids:
				if pmid.strip()!='':
					try:
						newpmid=int(pmid.strip())
						if newpmid not in pmid_list:
							pmid_list.append(newpmid)
					except ValueError:
						print "not an int"

	elif argpmids!='':
		ps=argpmids.split(',')
		try:
			pmid_list=map(int, ps)
			found=True
			context_data_dict['found']=found
		except ValueError:
			print "not an int"


	
	# seach database for PMIDs and refine the pmid_list
	tuples=list()
	pmidDict=dict()

	
	additionals=Additional.objects.filter(pmid__in=Pmid.objects.filter(pmid__in=pmid_list))
	for additional in additionals:
		dataintuples=dict()
		pmidDict[additional.pmid.pmid]=additional.pmid.pmid
		dataintuples['pmid']=additional.pmid.pmid
		dataintuples['patient']=additional.patient
		dataintuples['population']=additional.population
		dataintuples['review']=additional.review
		dataintuples['meta']=additional.meta
		triplets = Triplets.objects.filter(pmid=Pmid.objects.get(pmid=additional.pmid.pmid))
		triplet_list=list()
		for triplet in triplets:
			temp_str=re.sub(r'&gt ;','>',triplet.mutation.mutation)+', '+triplet.gene.gene+', '+triplet.disease.disease
			triplet_list.append(temp_str)
		dataintuples['triplets']=triplet_list
		dataintuples['tripletsize']=len(triplet_list)
		tuples.append(dataintuples)

	stats=dict()
	stats['abstract_length']=len(pmid_list)
	stats['tuples_length']=len(tuples)
		
	context_data_dict['tuples']=tuples
	context_data_dict['stat']=stats

	# writing to CSV for downloading
	pwd=os.getcwd()
	filepath=pwd+'/static/downloadcsvpmid'
	#filepath=pwd+'/DocumentRoot-default/mahmood/dimex/mtod'+'/static/downloadcsvpmid'
	#f = open('/static/downloadcsvpmid','w')
	f = open(filepath,'w')
	line='PMID'+'\t'+'PATIENT'+'\t'+'POPULATION'+'\t'+'REVIEW'+'\t'+'META-ANALYSIS'+'\t'+'TRIPLETS'
	f.write(line+'\n')
	for dataintuples in tuples:
		alltriplets=""
		for triplet in dataintuples['triplets']:
			alltriplets+='< '+triplet+' >'+', '
		alltriplets=re.sub(r', $','',alltriplets)
		line=str(dataintuples['pmid'])+'\t'+dataintuples['patient']+'\t'+dataintuples['population']+'\t'+dataintuples['review']+'\t'+dataintuples['meta']+'\t'+alltriplets
		f.write(line+'\n')
	f.close()

	
	'''
	for pmid in pmid_list:
		dataintuples=dict()
		pmidobj = Pmid.objects.filter(pmid=pmid, isextracted=1)		
		triplets = Triplets.objects.filter(pmid=pmidobj)
		for triplet in triplets:
			dataintuples=dict()
			dataintuples['pmid']=pmid
			dataintuples['mut']=triplet.mutation.mutation
			dataintuples['gene']=triplet.gene.gene
			dataintuples['disease']=triplet.disease.disease
			dataintuples['location']=triplet.extractlocation
			tuples.append(dataintuples)
			pmidDict[pmid]=pmid


	stats=dict()
	stats['abstract_length']=len(pmidDict)
	stats['tuples_length']=len(tuples)
	context_data_dict['tuples']=tuples
	context_data_dict['stat']=stats
	'''

	html = t.render(Context(context_data_dict))
	return HttpResponse(html)


# eVarD contactpage
def sentenceviewpage(request, pmid):
	t = get_template('sentenceview.html')
	
	lowConfidenceMethods=['MA','TI','CO']
	noConfidenceMethods=['COMEN']

	context_data_dict=dict()

	tuples=list()
	pmidobj = Pmid.objects.filter(pmid=pmid, isextracted=1)		
	triplets = Triplets.objects.filter(pmid=pmidobj)
	for triplet in triplets:
		dataintuples=dict()
		dataintuples['pmid']=pmid
		dataintuples['mut']=re.sub(r'&gt ;','>',triplet.mutation.mutation)
		dataintuples['gene']=triplet.gene.gene
		dataintuples['entrez']=triplet.gene.entrezids
		dataintuples['entrezsymbol']=triplet.gene.entrezsymbol
		dataintuples['disease']=triplet.disease.disease
		dataintuples['doid']=triplet.disease.DOID
		dataintuples['doidname']=triplet.disease.DOIDname
		dataintuples['sentno']=int(triplet.extractsent)+1		
		dataintuples['isstudy']=triplet.isstudy
		dataintuples['isnegative']=triplet.isnegative
		dataintuples['exmethod']=triplet.extractmethod	

		negativeStr=''
		if dataintuples['isnegative']==1:
			negativeStr=' (NEG)'

		if dataintuples['isstudy']==1:
			dataintuples['confidence']='?'

		elif dataintuples['exmethod'] in noConfidenceMethods:
			dataintuples['confidence']='?'

		elif dataintuples['exmethod'] in lowConfidenceMethods:
			dataintuples['confidence']='LOW'+negativeStr
		else:
			dataintuples['confidence']='HIGH'+negativeStr	
		tuples.append(dataintuples)

	context_data_dict['tuples']=tuples
	context_data_dict['pmid']=pmid


	additional=Additional.objects.filter(pmid=pmidobj)[0]
	additional_info=list()
	if additional.review!='':
		additional_info.append("Article type: Review")
	if additional.meta!='':
		additional_info.append("Article type: Meta-analysis")
	if additional.patient!='':
		additional_info.append("Patients: "+additional.patient)
	if additional.population!='':
		additional_info.append("Population: "+additional.population)
	
	context_data_dict['additional']=additional_info


	#sentences
	sentences=Abstracts.objects.filter(pmid=pmidobj).order_by('sentno')
	sentencelist=list()
	for sent in sentences:
		sentencelist.append(sent.text)

	context_data_dict['taggedsentences']=sentencelist

	html = t.render(Context(context_data_dict))
	return HttpResponse(html)

def showtextpage(request, data):
	t = get_template('showtext.html')
	context_data_dict=dict()	

	tuples=list()

	pmids=re.split('~@~',data)
	for pmid in pmids:
		tuples.append(pmid)
	context_data_dict['tuples']=tuples

	html = t.render(Context(context_data_dict))
	return HttpResponse(html)