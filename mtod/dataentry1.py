import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mtod.settings")

#from evard.models import *
from evard.models import *



#pmid=Pmid.objects.create(pmid=19930591, isextracted=1)



#disease=Disease.objects.create(pmid=Pmid.objects.get(pmid=19930591), disease='colorectal cancer', normalizeddisease='COLORECTAL CANCER', sentno=10, startpos=0, endpos=0)

'''
gene=Gene.objects.create(pmid=Pmid.objects.get(pmid=19930591), \
					gene='BCRP', \
					normalizedgene='BCRP', \
					sentno=10, \
					startpos=0, \
					endpos=0)
'''

#Mutation.objects.create(pmid=Pmid.objects.get(pmid=19930591), mutation='rs3789243', normalizedmutation='RS3789243', wtype3='', mtype3='', wtype1='', mtype1='', mpos=0, sentno=7, startpos=0, endpos=0)
'''
mutation=Mutation.objects.create(pmid=Pmid.objects.get(pmid=19930591), \
								mutation='rs3789243', \
								normalizedmutation='RS3789243', \
								wtype3='', \
								mtype3='', \
								wtype1='', \
								mtype1='', \
								mpos=0, \
								sentno=7, \
								startpos=0, \
								endpos=0)
'''

#triplet=Triplets.objects.create(pmid=Pmid.objects.get(pmid=19930591), mutation=Mutation.objects.get(mutation='rs3789243', pmid=Pmid.objects.get(pmid=19930591)), gene=Gene.objects.get(gene='BCRP', pmid=Pmid.objects.get(pmid=19930591)), disease=Disease.objects.get(disease='colorectal cancer', pmid=Pmid.objects.get(pmid=19930591)), isnegative=0, extractlocation='Result', extractsent=7, extractmethod='ASSOC', outcome='') 

triplet=Triplets.objects.create(pmid=Pmid.objects.get(pmid=19930591), \
								mutation=Mutation.objects.get(mutation='rs3789243', pmid=Pmid.objects.get(pmid=19930591)), \
								gene=Gene.objects.get(gene='BCRP', pmid=Pmid.objects.get(pmid=19930591)), \
								disease=Disease.objects.get(disease='colorectal cancer', pmid=Pmid.objects.get(pmid=19930591)), \
								isnegative=0, \
								extractlocation='Result', \
								extractsent=7, \
								extractmethod='ASSOC', \
								outcome='') 



	
'''
additional=Additional.objects.create(pmid=Pmid.objects.get(pmid=19930591), \
								patient='', \
								population='Danish')
'''

'''
abstract=Abstracts.objects.create(pmid=Pmid.objects.get(pmid=19930591), \
									sentno=16, \
									sentcategory='CONCLUSION', \
									text='Our results suggest that MDR1 polymorphisms affect the relationship between meat and CRC risk.' )
'''